/*
 * Adrian Lin
 */

#ifdef __APPLE__
  #include <OpenGL/gl.h>
  #include <OpenGL/glu.h>
  #include <GLUT/glut.h>
#elif __linux__
  #include <GL/glut.h>
#endif

#include <math.h>
#include <stdio.h>
#include <string.h>

struct Vertex
{
    GLfloat x;
    GLfloat y;
    GLfloat z;
};

struct Face
{
    int vertexIndex1;
    int vertexIndex2;
    int vertexIndex3;
};

struct Vertex vertices[5000];
struct Face faces[5000];

int vertices_cursor;
int faces_cursor;

float camera_position_x, camera_position_y, camera_position_z;
float camera_angle;
float center_x, center_y, center_z;
float lx, lz;

int display_as_wireframe;

void init_light(void)
{
    glShadeModel(GL_SMOOTH);
    glClearColor(1.0f, 1.0f, 1.0f, 0.0f);               
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glEnable(GL_COLOR_MATERIAL);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    GLfloat lightPos[4] = {-1.0, 1.0, 0.5, 0.0};
    glLightfv(GL_LIGHT0, GL_POSITION, (GLfloat *) &lightPos);
    glEnable(GL_LIGHT1);
    GLfloat lightAmbient1[4] = {0.0, 0.0,  0.0, 0.0};
    GLfloat lightPos1[4]     = {1.0, 0.0, -0.2, 0.0};
    GLfloat lightDiffuse1[4] = {0.5, 0.5,  0.3, 0.0};
    glLightfv(GL_LIGHT1,GL_POSITION, (GLfloat *) &lightPos1);
    glLightfv(GL_LIGHT1,GL_AMBIENT, (GLfloat *) &lightAmbient1);
    glLightfv(GL_LIGHT1,GL_DIFFUSE, (GLfloat *) &lightDiffuse1);
    glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);
}

void init_camera(void)
{
    camera_position_x = 0.0f;
    camera_position_y = 0.0f;
    camera_position_z = 1.15f;

    center_x = 0.0f;
    center_y = 0.0f;
    center_z = 0.0f;

    lx = 0.0f;
    lz = -1.0f;

    camera_angle = 0.0f;
}

void init_display(void)
{
    display_as_wireframe = 1;
}

void display(void)
{
    // clear color and depth buffers
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if (display_as_wireframe)
    {
    glPolygonMode(GL_FRONT, GL_LINE);
        glPolygonMode(GL_BACK, GL_LINE);
    }
    else
    {
    glPolygonMode(GL_FRONT, GL_FILL);
        glPolygonMode(GL_BACK, GL_FILL);
    }

    // reset transformations
    glLoadIdentity();
    
    // set the camera
    gluLookAt(camera_position_x, camera_position_y, camera_position_z,
              camera_position_x + lx, center_y, camera_position_z + lz,
              0.0f, 1.0f, 0.0f);

    glTranslatef(0.0f, 0.0f, 0.0f);
    
    glPushMatrix();

    // another way to rotate (currently rotating the camera)
    // glRotatef(camera_angle, 0, 1, 0);
    
    glScalef(4, 4, 4);
  
    int i;
    for (i = 0; i < faces_cursor; i++)
    {            
        glBegin(GL_TRIANGLES);
        glVertex3f(vertices[faces[i].vertexIndex1 - 1].x,
                   vertices[faces[i].vertexIndex1 - 1].y,
                   vertices[faces[i].vertexIndex1 - 1].z);
        glVertex3f(vertices[faces[i].vertexIndex2 - 1].x,
                   vertices[faces[i].vertexIndex2 - 1].y,
                   vertices[faces[i].vertexIndex2 - 1].z);
        glVertex3f(vertices[faces[i].vertexIndex3 - 1].x,
                   vertices[faces[i].vertexIndex3 - 1].y,
                   vertices[faces[i].vertexIndex3 - 1].z);
        glEnd();
    }
  
    glPopMatrix();
    glutSwapBuffers();
    glutPostRedisplay();
}

void reshape(int w, int h)  
{
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION); 
    glLoadIdentity();  
    if (h == 0)  
    { 
        gluPerspective(80, (float) w, 1.0, 5000.0);
    }
    else
    {
        gluPerspective(80, (float) w / (float) h, 1.0, 5000.0);
    }
    glMatrixMode(GL_MODELVIEW);  
    glLoadIdentity(); 
}

int is_vertex_char(char c)
{
    if ((c >= '0' && c <= '9') || c == '-' || c == 'e' || c == '.')
    {
        return 1;
    }

    return 0;
}

int is_face_char(char c)
{
    if (c >= '0' && c <= '9')
    {
        return 1;
    }
    
    return 0;
}

void parse_vertex(char line[128])
{
    const int MAX_VERTEX_LENGTH = 100; // assume that a vertex will not be longer than 100 chars
    
    int vertices_struct_pos;
    int c_pos;
    char current_vertex[MAX_VERTEX_LENGTH];
    int current_vertex_pos;

    vertices_struct_pos = 0;
    c_pos = 2;
    current_vertex_pos = 0;

    while (vertices_struct_pos <= 2)
    {
        if (is_vertex_char(line[c_pos]))
        {
            current_vertex[current_vertex_pos] = line[c_pos];
            current_vertex_pos++;
        }
        else
        {
            GLfloat vertex;
            vertex = (GLfloat) atof(current_vertex);

            if (vertices_struct_pos == 0)
            {
                vertices[vertices_cursor].x = vertex;
            }
            else if (vertices_struct_pos == 1)
            {
                vertices[vertices_cursor].y = vertex;
            }
            else if (vertices_struct_pos == 2)
            {
                vertices[vertices_cursor].z = vertex;
            }
            
            // clear current_vertex
            int p;
            for (p = 0; p < MAX_VERTEX_LENGTH; p++)
            {
                current_vertex[p] = 0;
            }

            current_vertex_pos = 0;
            vertices_struct_pos++;
        }

        c_pos = c_pos + 1;
    }
}

void parse_face(char line[128])
{
    const int MAX_FACE_LENGTH = 100; // assume that a vertex will not be longer than 100 chars
    
    int faces_struct_pos;
    int c_pos;
    char current_face[MAX_FACE_LENGTH];
    int current_face_pos;

    faces_struct_pos = 0;
    c_pos = 2;
    current_face_pos = 0;

    // parse line
    while (faces_struct_pos <= 2)
    {
        if (is_face_char(line[c_pos]))
        {
            current_face[current_face_pos] = line[c_pos];
            current_face_pos++;
        }
        else
        {
            int vertexIndex;
            vertexIndex = atoi(current_face);

            if (faces_struct_pos == 0)
            {
                faces[faces_cursor].vertexIndex1 = vertexIndex;
            }
            else if (faces_struct_pos == 1)
            {
                faces[faces_cursor].vertexIndex2 = vertexIndex;
            }
            else if (faces_struct_pos == 2)
            {
                faces[faces_cursor].vertexIndex3 = vertexIndex;
            }
            
            // clear current_face
            int p;
            for (p = 0; p < MAX_FACE_LENGTH; p++)
            {
                current_face[p] = 0;
            }

            current_face_pos = 0;
            faces_struct_pos++;
        }

        c_pos = c_pos + 1;
    }
}

void print_vertices()
{
    int i;
    for (i = 0; i < vertices_cursor; i++)
    {
        printf("%f %f %f\n", vertices[i].x, vertices[i].y, vertices[i].z);
    }
}

void print_faces()
{
    int i;
    for (i = 0; i < faces_cursor; i++)
    {
        printf("%d %d %d\n", faces[i].vertexIndex1, faces[i].vertexIndex2, faces[i].vertexIndex3);
    }
}

void parse_obj_file()
{
    static char FILENAME[] = "bunny.obj";
    static const int MAX_LINE_CHAR = 128;
    
    FILE *fp;
    int i;
    
    vertices_cursor = 0;
    faces_cursor = 0;
    
    fp = fopen(FILENAME, "r");
    if (fp != NULL)
    {
        char line[MAX_LINE_CHAR];
    
        // visit each line
        while (fgets(line, sizeof line, fp) != NULL)
        {
            if (line[0] == 'v')
            {
                parse_vertex(line);
                vertices_cursor++;
            }
            else if (line[0] == 'f')
            {
                parse_face(line);
                faces_cursor++;
            }
        }
    
        fclose(fp);

        //print_vertices();
        //print_faces();
    }
    else
    {
        printf("Error while opening %s", FILENAME);
    }
}

void keyboard(unsigned char key, int x, int y)
{
    switch(key)
    {
    case 27:
        exit(0);
        printf("Close window\n");
    break;
    case 'w':
        display_as_wireframe = 1;
        printf("Display as wireframe\n");
    break;
    case 's':
        display_as_wireframe = 0;
        printf("Display as solid body model\n");
    break;
    }
}

void arrow_keys(int key, int x, int y)
{
    float delta = 0.05f;
    
    switch(key)
    {
    case GLUT_KEY_UP:
        camera_position_x += lx * delta;
        camera_position_z += lz * delta;
    break;
    case GLUT_KEY_DOWN:
        camera_position_x -= lx * delta;
        camera_position_z -= lz * delta;
    break;
    case GLUT_KEY_LEFT:
        camera_angle -= 0.01f;
        lx = sin(camera_angle);
        lz = -cos(camera_angle);
    break;
    case GLUT_KEY_RIGHT:
        camera_angle += 0.01f;
        lx = sin(camera_angle);
        lz = -cos(camera_angle);
    break;
    }
}

int main(int argc, char *argv[]) 
{
    const int WINDOW_WIDTH = 1280;
    const int WINDOW_HEIGHT = 720;
    const char *WINDOW_TITLE = "OBJ Visualizer";
    
    parse_obj_file();  

    // init GLUT and create window
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_ALPHA | GLUT_DEPTH);
    glutInitWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT); 
    glutInitWindowPosition(0, 0);
    glutCreateWindow(WINDOW_TITLE);
    
    init_light();
    init_camera();
    init_display();

    // register callbacks
    glutDisplayFunc(display);  
    glutReshapeFunc(reshape);
    glutKeyboardFunc(keyboard);
    glutSpecialFunc(arrow_keys);

    // enter GLUT event processing cycle
    glutMainLoop();
}
